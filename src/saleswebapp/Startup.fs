namespace Test

open global.Owin
open Microsoft.Owin
open System.Web.Http
open Microsoft.Owin.Cors

type RouteParam = 
    { controller : string
      id : System.Object }

type Startup() = 
    member __.Configuration(builder : IAppBuilder) = 
        builder.UseCors(CorsOptions.AllowAll) |> ignore
        let config = new HttpConfiguration()
        // Configure routing
        config.Routes.MapHttpRoute("DefaultAPI", "api/{controller}/{id}", 
                                   { controller = "Home"
                                     id = RouteParameter.Optional }
                                   |> box)
        |> ignore
        config.MapHttpAttributeRoutes()
        // Configure serialization
        config.Formatters.XmlFormatter.UseXmlSerializer <- true
        config.Formatters.JsonFormatter.SerializerSettings.ContractResolver <- Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver
                                                                                   ()
        // Additional Web API settings
        config.EnsureInitialized()
        builder.UseWebApi(config) |> ignore

[<assembly:OwinStartup(typeof<Startup>)>]
do ()
