var Sales = angular.module('Sales', ['ngRoute']);

Sales.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/login', {
            templateUrl: 'Views/login.html',
            controller: 'loginController'
        })
        .when('/cars', {
            templateUrl: 'Views/car.html',
            controller: 'carController'
        })
        .when('/carAdd', {
            templateUrl: 'Views/carAdd.html',
            controller: 'carAddController'
        })
        .when('/carEdit', {
            templateUrl: 'Views/carEdit.html',
            controller: 'carEditController'
        })
        .when('/carView', {
            templateUrl: 'Views/carView.html',
            controller: 'carViewController'
        })
        .when('/sales', {
            templateUrl: 'Views/sales.html',
            controller: 'salesController'
        })
        .when('/sell', {
            templateUrl: 'Views/sell.html',
            controller: 'sellController'
        })
        .when('/salesView', {
            templateUrl: 'Views/salesView.html',
            controller: 'salesViewController'
        })
        .when('/trades', {
            templateUrl: 'Views/trades.html',
            controller: 'tradesController'
        })
        .when('/trade', {
            templateUrl: 'Views/trade.html',
            controller: 'tradeController'
        })
        .when('/tradesView', {
            templateUrl: 'Views/tradesView.html',
            controller: 'tradesViewController'
        })
        .when('/manageTestDrive', {
            templateUrl: 'Views/testDriveManage.html',
            controller: 'manageTestDriveController'
        })
        .when('/bookTestDrive', {
            templateUrl: 'Views/testDriveBook.html',
            controller: 'bookTestDriveController'
        })
        .when('/manageServices', {
            templateUrl: 'Views/serviceManage.html',
            controller: 'manageServicesController'
        })
        .when('/bookService', {
            templateUrl: 'Views/serviceBook.html',
            controller: 'bookServicesController'
        })
        .when('/viewService', {
            templateUrl: 'Views/serviceView.html',
            controller: 'viewServicesController'
        })
        .when('/expenses', {
            templateUrl: 'Views/expenses.html',
            controller: 'expensesController'
        })
        .when('/expensesFile', {
            templateUrl: 'Views/expensesFile.html',
            controller: 'expensesFileController'
        })
        .when('/invoices', {
            templateUrl: 'Views/invoices.html',
            controller: 'invoicesController'
        })
        .when('/balance', {
            templateUrl: 'Views/balance.html',
            controller: 'balanceController'
        })
        .when('/staffMembers', {
            templateUrl: 'Views/staffMembers.html',
            controller: 'staffMembersController'
        })
        .when('/staffMembersAdd', {
            templateUrl: 'Views/staffMembersAdd.html',
            controller: 'staffMemberAddController'
        })
        .when('/myAccount', {
            templateUrl: 'Views/myAccount.html',
            controller: 'myAccountController'
        })
        .when('/editAccount', {
            templateUrl: 'Views/myAccountEdit.html',
            controller: 'editAccountController'
        })
        .when('/createCustomerAccount', {
            templateUrl: 'Views/createCustomerAccount.html',
            controller: 'createCustomerAccountController'
        })
        .when('/apologies', {
            templateUrl: 'Views/apologiesView.html'
        })
        .otherwise({
            redirectTo: '/login'
        });
}]);

Sales.controller('loginController', function ($scope, $http) {

    $("#logres").text('Enter UserName and Password');

    $("#login-form").validate(); //Jquery validate

    $("#usernameinput").rules('add', {
        required: true
    });

    $("#passwordinput").rules('add', {
        required: true
    });

    $("#login-form").validate();

    $('#loginButton').click(function () {

        var uName = $("#usernameinput").val();
        var pass = $("#passwordinput").val();

        $.ajax({
            method: "POST",
            url: 'https://teesmo-profilecomponent.azurewebsites.net/SP/login',
            dataType: 'Json',
            data: '{"StaffUserName": "' + uName + '","StaffPassword": "' + pass + '"}',
            success: function (userData) {
                console.log(userData);
                if (userData[0].status === "OK") {
                    $("#logres").text("");
                    $("#logres").text('UserName and Password good');

                    var secretKey = makeid();
                    var encryptedPassword = CryptoJS.AES.encrypt(pass, secretKey);

                    sessionStorage.setItem("userData", JSON.stringify(userData));
                    sessionStorage.setItem("password", encryptedPassword);
                    sessionStorage.setItem("secretKey", secretKey);

                    console.log(userData);
                    window.location.href = '#!/cars';
                }
                else if (userData[0].status === "PASSWORD") {
                    $("#logres").text("");
                    $("#logres").text('Incorrect Password');

                }
                else if (userData[0].status === "INACTIVE") {
                    $("#logres").text("");
                    $("#logres").text('Account is Inactive');

                }
                else if (userData[0].status === "WRONG") {
                    $("#logres").text("");
                    $("#logres").text('Account does not exist');

                }
            }
        });
    });

});

Sales.controller('carController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        var data = '{"Make" : "","Model" : "","YearUpperBound" : 9999,"YearLowerBound" : 9999,"Colour" : "","BodyType" : "","Doors" : 10,"GearBoxType" : "","MilesUpperBound" : 1000000,"MilesLowerBound" : 1000000,"EngineSizeUpperBound" : 99.9,"EngineSizeLowerBound" : 99.9,"SixtyTimeUpperBound" : 99.9,"SixtyTimeLowerBound" :  99.9,"FuelType" : "","DriveTrainType" : "","Co2UpperBound" : 9999,"Co2LowerBound" : 9999, "PriceUpperBound" : 999999999.9,"PriceLowerBound" : 999999999.9}';

        $.ajax({
            method: "POST",
            url: 'https://teesmo-carcomponent.azurewebsites.net/cc/SearchAll',
            dataType: 'Json',
            data: data,
            success: function (results) {
                console.log(results);
                $('#searchPageResultsList')
                    .empty();

                $.each(results, function (index, t) {
                    var title = t.make + " " + t.model;

                    var data = t.year + " | " + t.bodyType + " | " + t.miles + "miles | " + t.gearBoxType + " | " + t.engineSize + "L | " + t.horsePower + "bhp | " + t.fuelType;

                    var price = '&pound' + t.price;

                    if (t.active.toString() === "false") {
                        price = "Sold";
                    }

                    $('#searchPageResultsList').append('<li><article><div id="searchResultdiv"><div id="searchResultImagediv"><img id="imageOutput' + t.carID + '"></div><div id="searchResultDatadiv"><h2><a id="viewCar-' + t.carID + '" href="#!/carView#type=viewCar&carID=' + t.carID + '">' + title + '</a></h2><p>' + data + '</p><p>' + price + '</p></div></div ></article></li>');

                    var output2 = document.getElementById('imageOutput' + t.carID);
                    output2.src = t.imageString;
                    output2.style.height = '200px';
                    output2.style.width = '270px';
                });

            }
        });

        $.ajax({
            url: 'https://teesmo-carcomponent.azurewebsites.net/cc/GetSearchAllParam',
            dataType: 'Json',
            success: function (results) {
                console.log(results)

                $.each(results.makes, function (index, t) {
                    $('#carSearch-Make').append($("<option></option>")
                        .val(t)
                        .text(t));
                });

                $.each(results.years, function (index, t) {
                    $('#carSearch-yearUpperBound').append($("<option></option>")
                        .val(t)
                        .text(t));
                });

                $.each(results.years, function (index, t) {
                    $('#carSearch-yearLowerBound').append($("<option></option>")
                        .val(t)
                        .text(t));
                });

                $.each(results.colour, function (index, t) {
                    $('#carSearch-Colour').append($("<option></option>")
                        .val(t)
                        .text(t));
                });

                $.each(results.miles, function (index, t) {
                    $('#carSearch-milesUpperBound').append($("<option></option>")
                        .val(t)
                        .text(t));
                });

                $.each(results.miles, function (index, t) {
                    $('#carSearch-milesLowerBound').append($("<option></option>")
                        .val(t)
                        .text(t));
                });

                $.each(results.engineSize, function (index, t) {
                    $('#carSearch-engineSizeUpperBound').append($("<option></option>")
                        .val(t)
                        .text(t));
                });

                $.each(results.engineSize, function (index, t) {
                    $('#carSearch-engineSizeLowerBound').append($("<option></option>")
                        .val(t)
                        .text(t));
                });

                $.each(results.co2, function (index, t) {
                    $('#carSearch-co2UpperBound').append($("<option></option>")
                        .val(t)
                        .text(t));
                });

                $.each(results.co2, function (index, t) {
                    $('#carSearch-co2LowerBound').append($("<option></option>")
                        .val(t)
                        .text(t));
                });

                $.each(results.price, function (index, t) {
                    $('#carSearch-priceUpperBound').append($("<option></option>")
                        .val(t)
                        .text(t));
                });

                $.each(results.price, function (index, t) {
                    $('#carSearch-priceLowerBound').append($("<option></option>")
                        .val(t)
                        .text(t));
                });
            }
        });


        $("#carSearch-Make")
            .change(function () {
                $("#carSearch-Make option:selected").each(function () {
                    console.log($(this).text());
                    var make = $(this).text();

                    $.ajax({
                        url: 'https://teesmo-carcomponent.azurewebsites.net/cc/GetModelSearchAllParam',
                        dataType: 'Json',
                        data: { make },
                        success: function (results) {
                            $('#carSearch-Model')
                                .empty()
                                .append('<option value="Any">Any</option>');

                            $.each(results, function (index, t) {
                                $('#carSearch-Model').append($("<option></option>")
                                    .val(t)
                                    .text(t));
                            });

                        }
                    });


                });
            })
            .change();

        $('#carParameterSearch').click(function () {

            var make = "";
            switch ($('#carSearch-Make').val()) {
                case "Any":
                    make = ""
                    break;
                default:
                    make = $('#carSearch-Make').val();
                    break;
            }

            var model = "";
            switch ($('#carSearch-Model').val()) {
                case "Any":
                    model = ""
                    break;
                default:
                    model = $('#carSearch-Model').val();
                    break;
            }

            var yearUpper = "";
            switch ($('#carSearch-yearUpperBound').val()) {
                case "Any":
                    yearUpper = "9999";
                    break;
                default:
                    yearUpper = $('#carSearch-yearUpperBound').val();
                    break;
            }

            var yearLower = "";
            switch ($('#carSearch-yearLowerBound').val()) {
                case "Any":
                    yearLower = "9999";
                    break;
                default:
                    yearLower = $('#carSearch-yearLowerBound').val();
                    break;
            }

            var colour = "";
            switch ($('#carSearch-Colour').val()) {
                case "Any":
                    colour = "";
                    break;
                default:
                    colour = $('#carSearch-Colour').val();
                    break;
            }

            var bodyType = "";
            switch ($('#carSearch-BodyType').val()) {
                case "Any":
                    bodyType = "";
                    break;
                default:
                    bodyType = $('#carSearch-BodyType').val();
                    break;
            }

            var doors = "";
            switch ($('#carSearch-Doors').val()) {
                case "Any":
                    doors = "10";
                    break;
                default:
                    doors = $('#carSearch-Doors').val();
                    break;
            }

            var gearBox = "";
            switch ($('#carSearch-GearBox').val()) {
                case "Any":
                    gearBox = ""
                    break;
                default:
                    gearBox = $('#carSearch-GearBox').val();
                    break;
            }

            var milesUpper = "";
            switch ($('#carSearch-milesUpperBound').val()) {
                case "Any":
                    milesUpper = "1000000";
                    break;
                default:
                    milesUpper = $('#carSearch-milesUpperBound').val();
                    break;
            }

            var milesLower = "";
            switch ($('#carSearch-milesLowerBound').val()) {
                case "Any":
                    milesLower = "1000000";
                    break;
                default:
                    milesLower = $('#carSearch-milesLowerBound').val();
                    break;
            }

            var engineSizeUpper = "";
            switch ($('#carSearch-engineSizeUpperBound').val()) {
                case "Any":
                    engineSizeUpper = "99.9";
                    break;
                default:
                    engineSizeUpper = $('#carSearch-engineSizeUpperBound').val();
                    break;
            }

            var engineSizeLower = "";
            switch ($('#carSearch-engineSizeLowerBound').val()) {
                case "Any":
                    engineSizeLower = "99.9";
                    break;
                default:
                    engineSizeLower = $('#carSearch-engineSizeLowerBound').val();
                    break;
            }

            var sixtyUpper = "";
            var sixtyLower = "";
            switch ($('#carSearch-0-60').val()) {
                case "Any":
                    var sixtyUpper = "99.9";
                    var sixtyLower = "99.9";
                    break;
                case "0-5":
                    var sixtyUpper = "5";
                    var sixtyLower = "0";
                    break;
                case "0-8":
                    var sixtyUpper = "8";
                    var sixtyLower = "0";
                    break;
                case "0-10":
                    var sixtyUpper = "10";
                    var sixtyLower = "0";
                    break;
                case "0-15":
                    var sixtyUpper = "15";
                    var sixtyLower = "0";
                    break;

            }

            var fuelType = "";
            switch ($('#carSearch-FuelType').val()) {
                case "Any":
                    fuelType = ""
                    break;
                default:
                    fuelType = $('#carSearch-FuelType').val();
                    break;
            }

            var driveTrainType = "";
            switch ($('#carSearch-DriveTrain').val()) {
                case "Any":
                    driveTrainType = ""
                    break;
                default:
                    driveTrainType = $('#carSearch-DriveTrain').val();
                    break;
            }

            var co2UpperBound = "";
            switch ($('#carSearch-co2UpperBound').val()) {
                case "Any":
                    co2UpperBound = "9999";
                    break;
                default:
                    co2UpperBound = $('#carSearch-co2UpperBound').val();
                    break;
            }

            var co2LowerBound = "";
            switch ($('#carSearch-co2LowerBound').val()) {
                case "Any":
                    co2LowerBound = "9999";
                    break;
                default:
                    co2LowerBound = $('#carSearch-co2LowerBound').val();
                    break;
            }

            var priceUpperBound = "";
            switch ($('#carSearch-priceUpperBound').val()) {
                case "Any":
                    priceUpperBound = "999999999.9";
                    break;
                default:
                    priceUpperBound = $('#carSearch-priceUpperBound').val();
                    break;
            }

            var priceLowerBound = "";
            switch ($('#carSearch-priceLowerBound').val()) {
                case "Any":
                    priceLowerBound = "999999999.9";
                    break;
                default:
                    priceLowerBound = $('#carSearch-priceLowerBound').val();
                    break;
            }

            var data = '{"Make" : "' + make +
                '","Model" : "' + model +
                '","YearUpperBound" : ' + parseInt(yearUpper) +
                ',"YearLowerBound" : ' + parseInt(yearLower) +
                ',"Colour" : "' + colour +
                '","BodyType" : "' + bodyType +
                '","Doors" : ' + parseInt(doors) +
                ',"GearBoxType" : "' + gearBox +
                '","MilesUpperBound" : ' + parseInt(milesUpper) +
                ',"MilesLowerBound" : ' + parseInt(milesLower) +
                ',"EngineSizeUpperBound" : ' + parseFloat(engineSizeUpper) +
                ',"EngineSizeLowerBound" : ' + parseFloat(engineSizeLower) +
                ',"SixtyTimeUpperBound" : ' + parseFloat(sixtyUpper) +
                ',"SixtyTimeLowerBound" :  ' + parseFloat(sixtyLower) +
                ', "FuelType" : "' + fuelType +
                '", "DriveTrainType" : "' + driveTrainType +
                '", "Co2UpperBound" : ' + parseInt(co2UpperBound) +
                ', "Co2LowerBound" : ' + parseInt(co2LowerBound) +
                ', "PriceUpperBound" : ' + parseFloat(priceUpperBound) +
                ', "PriceLowerBound" : ' + parseFloat(priceLowerBound) +
                '}';

            $.ajax({
                method: "POST",
                url: 'https://teesmo-carcomponent.azurewebsites.net/cc/SearchAll',
                dataType: 'Json',
                data: data,
                success: function (results) {
                    console.log(results);
                    $('#searchPageResultsList')
                        .empty();

                    $.each(results, function (index, t) {
                        var title = t.make + " " + t.model;

                        var data = t.year + " | " + t.bodyType + " | " + t.miles + "miles | " + t.gearBoxType + " | " + t.engineSize + "L | " + t.horsePower + "bhp | " + t.fuelType;

                        var price = '&pound' + t.price;

                        if (t.active.toString() === "false") {
                            price = "Sold";
                        }

                        $('#searchPageResultsList').append('<li><article><div id="searchResultdiv"><div id="searchResultImagediv"><img id="imageOutput' + t.carID + '"></div><div id="searchResultDatadiv"><h2><a id="viewCar-' + t.carID + '" href="#!/carView#type=viewCar&carID=' + t.carID + '">' + title + '</a></h2><p>' + data + '</p><p>' + price + '</p></div></div ></article></li>');

                        var output2 = document.getElementById('imageOutput' + t.carID);
                        output2.src = t.imageString;
                        output2.style.height = '200px';
                        output2.style.width = '270px';
                    });
                }
            });
        });
    }
});

Sales.controller('carAddController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {

        var location_vars = [];
        var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
        location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
        for (i = 0; i < location_vars_temp.length; i++) {
            location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
            location_vars.push(location_var_key_val[1]);
        }
        console.log(location_vars[0]);

        var input, canvas, context, output;
        input = document.getElementById("addCar-image");
        canvas = document.getElementById("canvas");
        context = canvas.getContext('2d');
        output = document.getElementById("output");

        input.addEventListener("change", function () {
            var reader = new FileReader();

            reader.addEventListener("loadend", function (arg) {
                var src_image = new Image();

                src_image.onload = function () {
                    canvas.height = src_image.height;
                    canvas.width = src_image.width;
                    context.drawImage(src_image, 0, 0);
                    var imageData = canvas.toDataURL("image/jpeg");
                    output.src = imageData;
                    console.log(imageData);
                }
                src_image.src = this.result;
            });
            reader.readAsDataURL(this.files[0]);
        });

        $("#addCarContainerDiv").validate({
            rules: {
                reg: {
                    required: true,
                    minlength: 3,
                    maxlength: 7
                },
                make: {
                    required: true
                },
                model: {
                    required: true
                },
                year: {
                    number: true,
                    minlength: 4,
                    required: true
                },
                colour: {
                    required: true
                },
                miles: {
                    number: true,
                    required: true
                },
                engineSize: {
                    number: true,
                    required: false
                },
                horsePower: {
                    number: true,
                    required: false
                },
                sixtyTime: {
                    number: true,
                    required: false
                },
                co2: {
                    number: true,
                    required: false
                },
                price: {
                    number: true,
                    required: true,
                    maxlength: 6
                }
            },
            messages: {
                reg: {
                    required: "Please enter the cars registration.",
                    minlength: "Must be at least 3 characters long.",
                    maxlength: "Must be no more than 7 characters long."
                },
                make: {
                    required: "Please enter the cars make."
                },
                model: {
                    required: "Please enter the cars model."
                },
                year: {
                    number: "Must be a number.",
                    minlength: "Must be 4 characters long.",
                    required: "Please enter the cars year."
                },
                colour: {
                    required: "Please enter the cars colour."
                },
                miles: {
                    number: "Must be a number.",
                    required: "Please enter how many miles the cars has."
                },
                engineSize: {
                    number: "Must be a number.",
                    required: "Please enter the cars engine size, in litres. (Not Required)"
                },
                horsePower: {
                    number: "Must be a number.",
                    required: "Please enter the cars horse power. (Not Required)"
                },
                sixtyTime: {
                    number: "Must be a number.",
                    required: "Please enter the cars 0-60 time. (Not Required)"
                },
                co2: {
                    number: "Must be a number.",
                    required: "Please enter the cars Co2 prduction in g/km. (Not Required)"
                },
                price: {
                    number: "Must be a number.",
                    required: "Please enter the price of the car."
                }
            }
        });

        $('#addCar-Button').click(function () {
            if ($("#addCarContainerDiv").valid() === false) {
                alert("Invalid Input");
            }
            else {
                var reg = $('#addCar-reg').val()
                var make = $('#addCar-make').val();
                var model = $('#addCar-model').val();
                var year = $('#addCar-year').val();
                var colour = $('#addCar-colour').val();
                var bodyType = $('#addCar-BodyType').val();
                var doors = $('#addCar-Doors').val();
                var gearbox = $('#addCar-GearBox').val();
                var miles = $('#addCar-miles').val();
                var engineSize = $('#addCar-engineSize').val();
                var horsePower = $('#addCar-horsePower').val();
                var sixtyTime = $('#addCar-sixtyTime').val();
                var fuel = $('#addCar-Fuel').val();
                var driveTrain = $('#addCar-driveTrain').val();
                var co2 = $('#addCar-co2').val();
                var price = $('#addCar-price').val();
                var dealership = $('#addCar-Dealership').val();
                var active = $('#addCar-Active').val();
                var imageData = canvas.toDataURL("image/jpeg");
                

                if (engineSize === "") {
                    engineSize = 0;
                }

                if (horsePower === "") {
                    horsePower = 0;
                }

                if (sixtyTime === "") {
                    sixtyTime = 0;
                }

                if (co2 === "") {
                    co2 = 0;
                }

                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));

                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-carcomponent.azurewebsites.net/cc/InsertNewCar',
                    dataType: 'Json',
                    data: '{"StaffUserName" : "' + userData[0].staffUserName + '","StaffPassword": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","Reg": "' + reg + '","Make": "' + make + '","Model": "' + model + '","Year": ' + parseInt(year) + ',"Colour": "' + colour +
                    '","BodyType": "' + bodyType + '","Doors": ' + parseInt(doors) + ',"GearBoxType": "' + gearbox + '","Miles": ' + parseInt(miles) + ',"EngineSize": ' + parseFloat(engineSize) + ',"HorsePower": ' + parseInt(horsePower) +
                    ',"SixtyTime": ' + parseFloat(sixtyTime) + ',"FuelType": "' + fuel + '","DriveTrainType": "' + driveTrain + '","Co2": ' + parseInt(co2) + ',"Price": ' + parseFloat(price) + ',"Dealership": ' + parseInt(dealership) + ',"Active": ' + active + ',"Image": "' + imageData + '"}',
                    success: function (results) {
                        if (location_vars[0] === undefined) {
                            window.location.href = '#!/cars';
                        } else if (location_vars[0] === "service") {
                            window.location.href = '#!/bookService';
                        }
                    }
                });
            }
        });
    }
});

Sales.controller('carEditController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {

        var location_vars = [];
        var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
        location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
        for (i = 0; i < location_vars_temp.length; i++) {
            location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
            location_vars.push(location_var_key_val[1]);
        }

        $("#editCarContainerDiv").validate({
            rules: {
                price: {
                    number: true,
                    required: true
                }
            },
            messages: {
                price: {
                    number: "Must be a number.",
                    required: "Please enter the price of the car."
                }
            }
        });

        var car;
        $.ajax({
            method: "POST",
            url: 'https://teesmo-carcomponent.azurewebsites.net/cc/FullCarDetails',
            dataType: 'Json',
            data: '{"carID" : ' + location_vars[1] + '}',
            success: function (results) {
                console.log(results);
                car = results[0];
                $('#editCar-Car').text(results[0].car);
                $('#editCar-price').val(results[0].price);
                $('#editCar-Dealership').val(results[0].dealershipID);
            }
        });

        $('#editCar-Button').click(function () {
            if ($("#addCarContainerDiv").valid() === false) {
                alert("Invalid Input");
            }
            else {
                var price = $('#editCar-price').val();
                var dealership = $('#editCar-Dealership').val();
                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-carcomponent.azurewebsites.net/cc/UpdateCar',
                    dataType: 'Json',
                    data: '{  "StaffUserName": "' + userData[0].staffUserName + '","StaffPassword": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","CarID": ' + car.carID + ',"Price": ' + parseFloat(price) +
                    ',"Sold": ' + car.sold + ',"Active": ' + car.active + ',"Dealership": ' + dealership + '}',
                    success: function (results) { window.location.href = '#!/cars'; }
                });
            }
        });
    }
});

Sales.controller('carViewController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        var location_vars = [];
        var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
        location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
        for (i = 0; i < location_vars_temp.length; i++) {
            location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
            location_vars.push(location_var_key_val[1]);
        }

        console.log(location_vars);

        $.ajax({
            method: "POST",
            url: 'https://teesmo-carcomponent.azurewebsites.net/cc/CarDetails',
            dataType: 'Json',
            data: '{"carID" : ' + location_vars[1] + '}',
            success: function (results) {
                console.log(results);

                $('#viewCar-title').text(results[0].make + " " + results[0].model);
                var book = '<a href="#!/bookTestDrive#type=carView&carID=' + results[0].carID + '" > Book Test Drive</a >'
                if (location_vars[0] === "viewYourCar") {
                    book = ''
                }


                $('#viewCarContainerDiv').empty();
                $('#viewCarContainerDiv').append('<div style="height:410px"><div class="viewCarImageDiv" id="viewCarImageDiv-' + results[0].carID + '"><img class="view-imageOutput" id="view-imageOutput' + results[0].carID + '"></div>\
                <div class="testDriveDiv" id= "testDriveDiv' + results[0].carID + '">\
                <a href="#!/carEdit#type=carEdit&carID=' + results[0].carID + '" > Edit Car</a >\
                </div ></div > <br />\
                <div class="viewCarDataDiv" id="viewCarDataDiv' + results[0].carID + '"><ul>' +
                    '<li class="carView_list" id="carView_list_year">' + results[0].year + '</li >' +
                    '<li class="carView_list" id="carView_list_bodyType">' + results[0].bodyType + '</li>' +
                    '<li class="carView_list" id="carView_list_miles">' + results[0].miles + 'miles</li>' +
                    '<li class="carView_list" id="carView_list_engineSize">' + results[0].engineSize + 'L</li>' +
                    '<li class="carView_list" id="carView_list_gearbox">' + results[0].gearBoxType + '</li>' +
                    '<li class="carView_list" id="carView_list_fuel">' + results[0].fuelType + '</li>' +
                    '</ul >' +
                    '<ul>' +
                    '<li class="carView_list" id="carView_list_colour">' + results[0].colour + '</li>' +
                    '<li class="carView_list" id="carView_list_doors">' + results[0].doors + ' doors</li>' +
                    '<li class="carView_list" id="carView_list_sixtyTime">' + results[0].sixtyTime + 's</li>' +
                    '<li class="carView_list" id="carView_list_horsePower">' + results[0].horsePower + 'bhp</li>' +
                    '<li class="carView_list" id="carView_list_driveTrain">' + results[0].driveTrainType + '</li>' +
                    '<li class="carView_list" id="carView_list_co2">' + results[0].co2 + 'g/km</li>' +
                    '</ul></div>');

                var output2 = document.getElementById('view-imageOutput' + results[0].carID);
                output2.src = results[0].imageString;
                output2.style.height = '400px';
                output2.style.width = '550px';
            }
        });
    }
});

Sales.controller('salesController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        var customerID = userData[0].customerID;
        var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
        if (userData[0].position === "Admin") {
            $.ajax({
                method: "POST",
                url: 'https://teesmo-servicescomponent.azurewebsites.net/SS/All',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
                success: function (results) {
                    console.log(results);
                    $('#salesTable').empty();
                    $('#salesTable').append('<tr><tr><th>Car</th><th>Customer</th><th>Staff Member</th><th>Dealership</th><th>Date</th></tr></tr>');


                    $.each(results, function (index, t) {
                        $('#salesTable').append('<tr><td>' + t.carDescription + '</td><td>' + t.customerName + '</td><td>' + t.staffName + '</td><td>' + t.dealershipName + '</td><td>' + (t.date.split('T'))[0] + '</td><td><a href="#!/salesView#type=SalesID&salesID=' + t.saleID + '">View Sale</a></td></tr>');

                    });
                }
            });
        }
        else {
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-servicescomponent.azurewebsites.net/SS/ByDealership',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","DealershipName": "' + userData[0].dealershipName + '"}',
                success: function (results) {
                    console.log(results);
                    $('#salesTable').empty();
                    $('#salesTable').append('<tr><tr><th>Car</th><th>Customer</th><th>Staff Member</th><th>Dealership</th><th>Date</th></tr></tr>');


                    $.each(results, function (index, t) {
                        $('#salesTable').append('<tr><td>' + t.carDescription + '</td><td>' + t.customerName + '</td><td>' + t.staffName + '</td><td>' + t.dealershipName + '</td><td>' + (t.date.split('T'))[0] + '</td><td><a href="#!/salesView#type=SalesID&salesID=' + t.saleID + '">View Sale</a></td></tr>');

                    });
                }
            });
        }

    }
});

Sales.controller('sellController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        $("#sellCarContainerDiv").validate({
            rules: {
                customerSelect: {
                    required: true
                },
                agreedPrice: {
                    required: true,
                    number: true
                }
            },
            messages: {
                customerSelect: {
                    required: "Please select a customer."
                },
                agreedPrice: {
                    required: "Please enter the agreed price.",
                    number: "Must be a number."
                }
            }
        });

        $.ajax({
            method: "POST",
            url: 'https://teesmo-carcomponent.azurewebsites.net/CC/ByDealershipID',
            dataType: 'Json',
            data: '{  "Dealership": "' + userData[0].dealershipName + '"}',
            success: function (results) {
                console.log(results);
                $('#sellCar-CarSelect').empty();
                $('#sellCar-CarSelect').append('<option price="0" value="0">Please Select</option>');
                $.each(results, function (index, t) {
                    $('#sellCar-CarSelect').append('<option price="' + t.price + '" value="' + t.carID + '">' + t.car + '</option>');
                });
            }
        });

        $('#sellCar-CarSelect')
            .change(function () {
                $("#sellCar-CarSelect option:selected").each(function () {
                    console.log($(this).text());
                    var car = $(this).text();
                    var price = $(this).attr('price');

                    console.log(price);
                    $('#sellCar-Car').val($(this).text());
                    $('#sellCar-Price').val($(this).attr('price'));
                });
            })
            .change();


        $('#sellCar-FindCustomer').click(function () {
            var firstName = $('#sellCar-firstName').val();
            var lastName = $('#sellCar-LastName').val();
            var city = $('#sellCar-City').val();
            var postCode = $('#sellCar-PostCode').val();
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));

            $.ajax({
                method: "POST",
                url: 'https://teesmo-profilecomponent.azurewebsites.net/CP/search',
                dataType: 'Json',
                data: '{  "StaffUserName": "' + userData[0].staffUserName + '","StaffPassword": "' + decrypted.toString(CryptoJS.enc.Utf8) +
                '","FirstName": "' + firstName + '","LastName": "' + lastName +
                '","Town": "' + city + '","Postcode": "' + postCode + '"}',
                success: function (results) {
                    console.log(results);
                    $('#sellCar-CustomerSelect').empty();

                    $.each(results, function (index, t) {
                        $('#sellCar-CustomerSelect').append('<option value="' + t.customerID + '">' + t.customerUserName + '</option>');
                    });
                }
            });
        });


        $('#sellCar-Button').click(function () {
            if ($("#sellCarContainerDiv").valid() === false) {
                alert("Invalid Input");
            }
            else {
                var carID = $('#sellCar-CarSelect').find(":selected").val();
                var customerID = $('#sellCar-CustomerSelect').find(":selected").val();
                var staffID = userData[0].staffID;
                var dealership = userData[0].dealershipName;
                var amount = $('#sellCar-AgreedPrice').val();

                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-servicescomponent.azurewebsites.net/SS/InsertSale',
                    dataType: 'Json',
                    data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) +
                    '","CarID": "' + carID + '","CustomerID": "' + customerID +
                    '","StaffID": "' + staffID + '","Dealership": "' + dealership + '","Amount": "' + amount + '"}',
                    success: function (results) {
                        window.location.href = '#!/sales';
                    }
                });
            }
        });
    }
});

Sales.controller('salesViewController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        if (userData[0].position === "Sales") {
            alert("Not Authorised");
            window.location.href = '#!/sales';
        }
        else {
            var location_vars = [];
            var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
            location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
            for (i = 0; i < location_vars_temp.length; i++) {
                location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
                location_vars.push(location_var_key_val[1]);
            }
            if (location_vars[0] === "SalesID") {
                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-servicescomponent.azurewebsites.net/SS/ByID',
                    dataType: 'Json',
                    data: '{  "UserName": "' + userData[0].staffUserName + '", "Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '", "SalesID": ' + location_vars[1] + ' }',
                    success: function (results) {
                        console.log(results);
                        $('#viewSaleContainerDiv').empty();
                        $('#viewSaleContainerDiv').append('<p>Car: <a id="viewCar-' + results[0].carID + '" href="#!/carView#type=viewCar&carID=' + results[0].carID + '">' + results[0].carDescription + '</a></p>\
                        <p>Amount: &pound'+ results[0].amount + '</p>\
                        <p>Customer: '+ results[0].customerName + '</p>\
                        <p>Staff Member: '+ results[0].staffName + '</p>\
                        <p>Location: '+ results[0].dealershipName + '</p>\
                        <p>Date: '+ (results[0].date.split("T"))[0] + '</p>');
                    }
                });
            }
            else if (location_vars[0] === "InvoiceID") {
                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-servicescomponent.azurewebsites.net/SS/ByInvoice',
                    dataType: 'Json',
                    data: '{  "UserName": "' + userData[0].staffUserName + '", "Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '", "InvoiceID": ' + location_vars[1] + ' }',
                    success: function (results) {
                        console.log(results);
                        $('#viewSaleContainerDiv').empty();
                        $('#viewSaleContainerDiv').append('<p>Car: <a id="viewCar-' + results[0].carID + '" href="#!/carView#type=viewCar&carID=' + results[0].carID + '">' + results[0].carDescription + '</a></p>\
                        <p>Amount: &pound'+ results[0].amount + '</p>\
                        <p>Customer: '+ results[0].customerName + '</p>\
                        <p>Staff Member: '+ results[0].staffName + '</p>\
                        <p>Location: '+ results[0].dealershipName + '</p>\
                        <p>Date: '+ (results[0].date.split("T"))[0] + '</p>');
                    }
                });
            }
        }
    }
});

Sales.controller('tradesController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        if (userData[0].position === "Admin") {
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-servicescomponent.azurewebsites.net/TS/All',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
                success: function (results) {
                    console.log(results);
                    $('#tradesTable').empty();
                    $('#tradesTable').append('<tr><th>Sold Car</th><th>Trade Car</th><th>Customer</th><th>Staff Member</th><th>Dealership</th><th>Date</th></tr>');


                    $.each(results, function (index, t) {
                        $('#tradesTable').append('<tr><td>' + t.soldCarDescription + '</td><td>' + t.tradeCarDescription + '</td><td>' + t.customerName + '</td><td>' + t.staffName + '</td><td>' + t.dealershipName + '</td><td>' + (t.date.split('T'))[0] + '</td><td><a href="#!/tradesView#type=TradeID&tradesID=' + t.tradeID + '">View Trade</a></td></tr>');

                    });
                }
            });
        }
        else {
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-servicescomponent.azurewebsites.net/TS/ByDealership',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","DealershipName": "' + userData[0].dealershipName + '"}',
                success: function (results) {
                    console.log(results);
                    $('#tradesTable').empty();
                    $('#tradesTable').append('<tr><th>Sold Car</th><th>Trade Car</th><th>Customer</th><th>Staff Member</th><th>Dealership</th><th>Date</th></tr>');


                    $.each(results, function (index, t) {
                        $('#tradesTable').append('<tr><td>' + t.soldCarDescription + '</td><td>' + t.tradeCarDescription + '</td><td>' + t.customerName + '</td><td>' + t.staffName + '</td><td>' + t.dealershipName + '</td><td>' + (t.date.split('T'))[0] + '</td><td><a href="#!/tradesView#type=TradeID&tradeID=' + t.tradeID + '">View Trade</a></td></tr>');

                    });
                }
            });
        }

    }
});

Sales.controller('tradeController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        $("#tradeCarContainerDiv").validate({
            rules: {
                customerSelect: {
                    required: true
                },
                agreedPrice1: {
                    required: true,
                    number: true
                },
                agreedPrice2: {
                    required: true,
                    number: true
                }
            },
            messages: {
                customerSelect: {
                    required: "Please select a customer."
                },
                agreedPrice1: {
                    required: "Please enter the agreed price.",
                    number: "Must be a number."
                },
                agreedPrice2: {
                    required: "Please enter the agreed price.",
                    number: "Must be a number."
                }
            }
        });

        $.ajax({
            method: "POST",
            url: 'https://teesmo-carcomponent.azurewebsites.net/CC/ByDealershipID',
            dataType: 'Json',
            data: '{  "Dealership": "' + userData[0].dealershipName + '"}',
            success: function (results) {
                console.log(results);
                $('#tradeCar-SoldCarSelect').empty();
                $('#tradeCar-SoldCarSelect').append('<option price="0" value="0">Please Select</option>');
                $.each(results, function (index, t) {
                    $('#tradeCar-SoldCarSelect').append('<option price="' + t.price + '" value="' + t.carID + '">' + t.car + '</option>');
                });
            }
        });

        $('#tradeCar-SoldCarSelect')
            .change(function () {
                $("#tradeCar-SoldCarSelect option:selected").each(function () {
                    console.log($(this).text());
                    var car = $(this).text();
                    var price = $(this).attr('price');

                    console.log(price);
                    $('#tradeCar-SoldCar').val($(this).text());
                    $('#tradeCar-SoldPrice').val($(this).attr('price'));
                });
            })
            .change();

        $('#tradeCar-findTradeCar').click(function () {
            var reg = $('#tradeCar-regInput').val();

            $.ajax({
                method: "POST",
                url: 'https://teesmo-carcomponent.azurewebsites.net/CC/ByReg',
                dataType: 'Json',
                data: '{  "Reg": "' + reg + '"}',
                success: function (results) {
                    console.log(results);
                    $('#tradeCar-TradeCarID').val(results[0].carID);
                    $('#tradeCar-TradeCar').val(results[0].car);
                    $('#tradeCar-TradePrice').val(results[0].price);
                }
            });
        });

        $('#tradeCar-FindCustomer').click(function () {
            var firstName = $('#tradeCar-firstName').val();
            var lastName = $('#tradeCar-LastName').val();
            var city = $('#tradeCar-City').val();
            var postCode = $('#tradeCar-PostCode').val();
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-profilecomponent.azurewebsites.net/CP/search',
                dataType: 'Json',
                data: '{  "StaffUserName": "' + userData[0].staffUserName + '","StaffPassword": "' + decrypted.toString(CryptoJS.enc.Utf8) +
                '","FirstName": "' + firstName + '","LastName": "' + lastName +
                '","Town": "' + city + '","Postcode": "' + postCode + '"}',
                success: function (results) {
                    console.log(results);
                    $('#tradeCar-CustomerSelect').empty();

                    $.each(results, function (index, t) {
                        $('#tradeCar-CustomerSelect').append('<option value="' + t.customerID + '">' + t.customerUserName + '</option>');
                    });
                }
            });
        });


        $('#tradeCar-Button').click(function () {
            if ($("#tradeCarContainerDiv").valid() === false) {
                alert("Invalid Input");
            }
            else {
                var soldCarID = $('#tradeCar-SoldCarSelect').find(":selected").val();
                var tradeCarID = $('#tradeCar-TradeCarID').val();
                var customerID = $('#tradeCar-CustomerSelect').find(":selected").val();
                var staffID = userData[0].staffID;
                var dealership = userData[0].dealershipName;

                var tradeAmount = $('#tradeCar-TradeAgreedPrice').val();
                var soldAmount = $('#tradeCar-SoldAgreedPrice').val();

                var finalAmount = parseFloat(soldAmount) - parseFloat(tradeAmount);

                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));

                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-servicescomponent.azurewebsites.net/TS/InsertTrade',
                    dataType: 'Json',
                    data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) +
                    '","SoldCarID": "' + parseInt(soldCarID) + '","TradeCarID": "' + parseInt(tradeCarID) + '","CustomerID": "' + parseInt(customerID) +
                    '","StaffID": "' + parseInt(staffID) + '","DealershipID": "' + dealership + '","Amount": "' + parseFloat(finalAmount) + '","Notes": "Traded"}',


                    success: function (results) {
                        window.location.href = '#!/trades';
                    }
                });
            }
        });
    }
});

Sales.controller('tradesViewController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        if (userData[0].position === "Sales") {
            alert("Not Authorised");
            window.location.href = '#!/sales';
        }
        else {
            var location_vars = [];
            var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
            location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
            for (i = 0; i < location_vars_temp.length; i++) {
                location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
                location_vars.push(location_var_key_val[1]);
            }
            if (location_vars[0] === "TradeID") {
                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-servicescomponent.azurewebsites.net/TS/ByID',
                    dataType: 'Json',
                    data: '{  "UserName": "' + userData[0].staffUserName + '", "Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '", "TradeID": "' + location_vars[1] + '" }',
                    success: function (results) {
                        console.log(results);
                        $('#viewTradeContainerDiv').empty();
                        $('#viewTradeContainerDiv').append('<p>Sold Car: <a id="viewCar-' + results[0].soldCarID + '" href="#!/carView#type=viewCar&carID=' + results[0].soldCarID + '">' + results[0].soldCarDescription + '</a></p>\
                        <p>Trade Car: <a id="viewCar-' + results[0].tradeCarID + '" href="#!/carView#type=viewCar&carID=' + results[0].tradeCarID + '">' + results[0].tradeCarDescription + '</a></p>\
                        <p>Amount: &pound'+ results[0].amount + '</p>\
                        <p>Customer: '+ results[0].customerName + '</p>\
                        <p>Staff Member: '+ results[0].staffName + '</p>\
                        <p>Location: '+ results[0].dealershipName + '</p>\
                        <p>Date: '+ (results[0].date.split("T"))[0] + '</p>');
                    }
                });
            }
            else if (location_vars[0] === "InvoiceID") {
                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-servicescomponent.azurewebsites.net/TS/ByInvoice',
                    dataType: 'Json',
                    data: '{  "UserName": "' + userData[0].staffUserName + '", "Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '", "TradeID": "' + location_vars[1] + '" }',
                    success: function (results) {
                        console.log(results);
                        $('#viewTradeContainerDiv').empty();
                        $('#viewTradeContainerDiv').append('<p>Sold Car: <a id="viewCar-' + results[0].soldCarID + '" href="#!/carView#type=viewCar&carID=' + results[0].soldCarID + '">' + results[0].soldCarDescription + '</a></p>\
                        <p>Trade Car: <a id="viewCar-' + results[0].tradeCarID + '" href="#!/carView#type=viewCar&carID=' + results[0].tradeCarID + '">' + results[0].tradeCarDescription + '</a></p>\
                        <p>Amount: &pound'+ results[0].amount + '</p>\
                        <p>Customer: '+ results[0].customerName + '</p>\
                        <p>Staff Member: '+ results[0].staffName + '</p>\
                        <p>Location: '+ results[0].dealershipName + '</p>\
                        <p>Date: '+ (results[0].date.split("T"))[0] + '</p>');
                    }
                });
            }
        }
    }
});

Sales.controller('manageTestDriveController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        console.log(userData);
        if (userData[0].position === "Admin") {
            AdminManageTestDrives(userData);
        }
        else {
            managementManageTestDrives(userData);
        }
    }
});

function AdminManageTestDrives(userData) {
    var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
    $.ajax({
        method: "POST",
        url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/All',
        dataType: 'Json',
        data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
        success: function (results) {
            console.log(results);
            $('#manageTestDrivesTable').empty();
            $('#manageTestDrivesTable').append('<tr><th>Test Drive ID</th><th>Customer</th><th>Car</th><th>Accompanying Staff Member</th><th>Date</th><th>Edit</th><th>Cancel</th></tr>');


            $.each(results, function (index, t) {
                $('#manageTestDrivesTable').append('<tr><td>' + t.testDriveBookingID + '</td><td>' + t.customerName + '</td><td>' + t.car + '</td><td>' + t.staffName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/bookTestDrive#type=editExisting&testdriveID=' + t.testDriveBookingID + '">Edit Test Drive</a></td><td><a id="cancelTestDriveBooking-' + t.testDriveBookingID + '">Cancel Booking</a></td></tr>');

                $('#cancelTestDriveBooking-' + t.testDriveBookingID).click(function () {
                    var accept = confirm("Do you want to continue?");
                    if (accept) {
                        $.ajax({
                            method: "POST",
                            url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/UpdateTestDriveBooking',
                            dataType: 'Json',
                            data: '{"TestDriveBookingID" : ' + t.testDriveBookingID + ',"CustomerID": ' + t.customerID + ',"CarID": ' + t.carID + ',"StaffID": ' + t.staffID + ',"Name": "' + t.name + '","DateBooked": "' + t.dateBooked + '","Active": false}',
                            success: function (results) {
                                AdminManageTestDrives(userData);
                            }
                        });
                    }

                });
            });
        }
    });
}

function managementManageTestDrives(userData) {
    var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
    $.ajax({
        method: "POST",
        url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/ByDealership',
        dataType: 'Json',
        data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","DealershipName": "' + userData[0].dealershipName + '"}',
        success: function (results) {
            console.log(results);
            $('#manageTestDrivesTable').empty();
            $('#manageTestDrivesTable').append('<tr><th>Test Drive ID</th><th>Customer</th><th>Car</th><th>Accompanying Staff Member</th><th>Date</th><th>Edit</th><th>Cancel</th></tr>');


            $.each(results, function (index, t) {
                $('#manageTestDrivesTable').append('<tr><td>' + t.testDriveBookingID + '</td><td>' + t.customerName + '</td><td>' + t.car + '</td><td>' + t.staffName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/bookTestDrive#type=editExisting&testdriveID=' + t.testDriveBookingID + '">Edit Test Drive</a></td><td><a id="cancelTestDriveBooking-' + t.testDriveBookingID + '">Cancel Booking</a></td></tr>');

                $('#cancelTestDriveBooking-' + t.testDriveBookingID).click(function () {
                    var accept = confirm("Do you want to continue?");
                    if (accept) {
                        $.ajax({
                            method: "POST",
                            url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/UpdateTestDriveBooking',
                            dataType: 'Json',
                            data: '{"TestDriveBookingID" : ' + t.testDriveBookingID + ',"CustomerID": ' + t.customerID + ',"CarID": ' + t.carID + ',"StaffID": ' + t.staffID + ',"Name": "' + t.name + '","DateBooked": "' + t.dateBooked + '","Active": false}',
                            success: function (results) {
                                managementManageTestDrives(userData);
                            }
                        });
                    }
                });
            });
        }
    });
}

Sales.controller('bookTestDriveController', function ($scope, $http) {
    var existData = "";
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {

        var date = new Date();
        var days = new Date(2018, (date.getMonth() + 1), 0).getDate();

        console.log(days);

        $('#bookTestDrive-Month').empty();
        for (var i = (date.getMonth() + 1); i < 13; i++) {
            if (i.toString().length === 1) {
                $('#bookTestDrive-Month').append('<option value="0' + i + '">0' + i + '</option>');
            }
            else {
                $('#bookTestDrive-Month').append('<option value="' + i + '">' + i + '</option>');
            }

        }

        $('#bookTestDrive-Day').empty();
        for (var i = (date.getDate() + 1); i < days + 1; i++) {
            if (i.toString().length === 1) {
                $('#bookTestDrive-Day').append('<option value="0' + i + '">0' + i + '</option>');
            }
            else {
                $('#bookTestDrive-Day').append('<option value="' + i + '">' + i + '</option>');
            }

        }

        $("#bookTestDriveForm").validate({
            rules: {
                customerSelect: {
                    required: true
                }
            },
            messages: {
                customerSelect: {
                    required: "Please select a customer."
                }
            }
        });

        var data = '{"Make" : "","Model" : "","YearUpperBound" : 9999,"YearLowerBound" : 9999,"Colour" : "","BodyType" : "","Doors" : 10,"GearBoxType" : "","MilesUpperBound" : 1000000,"MilesLowerBound" : 1000000,"EngineSizeUpperBound" : 99.9,"EngineSizeLowerBound" : 99.9,"SixtyTimeUpperBound" : 99.9,"SixtyTimeLowerBound" :  99.9,"FuelType" : "","DriveTrainType" : "","Co2UpperBound" : 9999,"Co2LowerBound" : 9999, "PriceUpperBound" : 999999999.9,"PriceLowerBound" : 999999999.9}';

        var ajax = $.ajax({
            method: "POST",
            url: 'https://teesmo-carcomponent.azurewebsites.net/cc/Search',
            dataType: 'Json',
            data: data,
            success: function (results) {
                $.each(results, function (index, t) {
                    $('#bookTestDrive-Car').append('<option value=' + t.carID + '>' + t.make + ' ' + t.model + ' ' + t.year + '</option>');
                });
            }
        });

        var location_vars = [];
        var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
        location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
        for (i = 0; i < location_vars_temp.length; i++) {
            location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
            location_vars.push(location_var_key_val[1]);
        }

        if (location_vars[0] == undefined) {
            console.log("No Car ID");
        }
        else if (location_vars[0] == "carView") {
            console.log("carView");
            $.when(ajax).done(function () {
                $('#bookTestDrive-Car').val(location_vars[1]);
            });
        }
        else if (location_vars[0] == "editExisting") {
            console.log("editExisting");
            $("#bookTestDrive-firstName").prop('disabled', true);
            $("#bookTestDrive-LastName").prop('disabled', true);
            $("#bookTestDrive-City").prop('disabled', true);
            $("#bookTestDrive-PostCode").prop('disabled', true);
            $("#bookTestDrive-FindCustomer").prop('disabled', true);
            $("#bookTestDrive-CustomerSelect").prop('disabled', true);
            $.when(ajax).done(function () {
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/ById',
                    dataType: 'Json',
                    data: '{"testDriveBookingID" : ' + location_vars[1] + '}',
                    success: function (results) {
                        existData = results[0];
                        console.log(results[0]);
                        $('#bookTestDrive-Car').val(results[0].carID);
                        var dateBooked = (results[0].dateBooked).split("-");
                        console.log(dateBooked);
                        var day = dateBooked[2].substring(0, 2);
                        var time = dateBooked[2].split("T");
                        var timeCompo = time[1].split(":");
                        var month = dateBooked[1];
                        var hour = timeCompo[0];
                        var minute = timeCompo[1];

                        console.log(day);
                        console.log(month);
                        console.log(hour);
                        console.log(minute);

                        $('#bookTestDrive-Day').val(day);
                        $('#bookTestDrive-Month').val(month);
                        $('#bookTestDrive-Hour').val(hour);
                        $('#bookTestDrive-Minute').val(minute);
                    }
                });
            });
        }

        $('#bookTestDrive-FindCustomer').click(function () {
            var firstName = $('#bookTestDrive-firstName').val();
            var lastName = $('#bookTestDrive-LastName').val();
            var city = $('#bookTestDrive-City').val();
            var postCode = $('#bookTestDrive-PostCode').val();
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-profilecomponent.azurewebsites.net/CP/search',
                dataType: 'Json',
                data: '{  "StaffUserName": "' + userData[0].staffUserName + '","StaffPassword": "' + decrypted.toString(CryptoJS.enc.Utf8) +
                '","FirstName": "' + firstName + '","LastName": "' + lastName +
                '","Town": "' + city + '","Postcode": "' + postCode + '"}',
                success: function (results) {
                    console.log(results);
                    $('#bookTestDrive-CustomerSelect').empty();

                    $.each(results, function (index, t) {
                        $('#bookTestDrive-CustomerSelect').append('<option value="' + t.customerID + '">' + t.customerUserName + '</option>');
                    });
                }
            });
        });

        $('#bookTestDrive-button').click(function () {
            if ($("#bookTestDriveForm").valid() === false) {
                alert("Invalid Input");
            }
            else {
                if (location_vars[0] == "editExisting") {
                    console.log("editExisting");
                    var customerID = existData.customerID;
                    var carID = $('#bookTestDrive-Car').val();
                    var dateBooked = $('#bookTestDrive-Year').val() + "-" + $('#bookTestDrive-Month').val() + "-" + $('#bookTestDrive-Day').val() + ' ' + $('#bookTestDrive-Hour').val() + ":" + $('#bookTestDrive-Minute').val();
                    console.log(existData);
                    $.ajax({
                        method: "POST",
                        url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/UpdateTestDriveBooking',
                        dataType: 'Json',
                        data: '{"TestDriveBookingID" : ' + existData.testDriveBookingID + ',"CustomerID": ' + customerID + ',"CarID": ' + carID + ',"StaffID": ' + existData.staffID + ',"Name": "' + existData.name + '","DateBooked": "' + dateBooked + '","Active": true}',
                        success: function (results) { window.location.href = '#!/manageTestDrive'; }
                    });
                } else {

                    var customerID = $('#bookTestDrive-CustomerSelect').val();
                    var carID = $('#bookTestDrive-Car').val();
                    var dateBooked = $('#bookTestDrive-Year').val() + "-" + $('#bookTestDrive-Month').val() + "-" + $('#bookTestDrive-Day').val() + ' ' + $('#bookTestDrive-Hour').val() + ":" + $('#bookTestDrive-Minute').val();

                    console.log(customerID);
                    console.log(carID);
                    console.log(dateBooked);

                    $.ajax({
                        method: "POST",
                        url: 'https://teesmo-bookingcomponent.azurewebsites.net/TDB/InsertTestDriveBooking',
                        dataType: 'Json',
                        data: '{"CustomerID" : ' + parseInt(customerID) + ' , "CarID" : ' + parseInt(carID) + ' , "DateBooked" : "' + dateBooked.toString() + '"}',
                        success: function (results) { window.location.href = '#!/manageTestDrive'; }
                    });
                }
            }
        });
    }
});

Sales.controller('manageServicesController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        console.log(userData);

        if (userData[0].position === "Admin") {
            AdminManageServices(userData);
        }
        else {
            managementManageServices(userData);
        }
    }
});

function AdminManageServices(userData) {
    var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
    $.ajax({
        method: "POST",
        url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/All',
        dataType: 'Json',
        data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
        success: function (results) {
            console.log(results);
            $('#manageServicesTable').empty();
            $('#manageServicesTable').append('<tr><th>Service ID</th><th>Customer</th><th>Car</th><th>Dealership</th><th>Date</th><th></th><th></th></tr>');


            $.each(results, function (index, t) {

                if (t.completed === true) {
                    $('#manageServicesTable').append('<tr><td>' + t.serviceBookingID + '</td><td>' + t.customerName + '</td><td>' + t.car + '</td><td>' + t.dealershipName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/viewService#type=ServiceID&serviceID=' + t.serviceBookingID + '">View Service</a></td><td></td></tr>');

                } else {
                    $('#manageServicesTable').append('<tr><td>' + t.serviceBookingID + '</td><td>' + t.customerName + '</td><td>' + t.car + '</td><td>' + t.dealershipName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/bookService#type=editExisting&serviceID=' + t.serviceBookingID + '">Edit Service</a></td><td><a id="cancelServiceBooking-' + t.serviceBookingID + '">Cancel Booking</a></td></tr>');

                    $('#cancelServiceBooking-' + t.serviceBookingID).click(function () {
                        var accept = confirm("Do you want to continue?");
                        if (accept) {
                            $.ajax({
                                method: "POST",
                                url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/UpdateServiceBooking',
                                dataType: 'Json',
                                data: '{"ServiceBookingID" : ' + t.serviceBookingID + ',"CustomerID": ' + t.customerID + ',"CarID": ' + t.carID + ',"StaffID": ' + t.staffID + ',"DateBooked": "' + t.dateBooked + '", "DealershipName" : "' + t.dealershipName + '","Active": false}',
                                success: function (results) {
                                    AdminManageServices(userData);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

function managementManageServices(userData) {
    var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
    $.ajax({
        method: "POST",
        url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/ByDealership',
        dataType: 'Json',
        data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","DealershipName": "' + userData[0].dealershipName + '"}',
        success: function (results) {
            console.log(results);
            $('#manageServicesTable').empty();
            $('#manageServicesTable').append('<tr><th>Service ID</th><th>Customer</th><th>Car</th><th>Dealership</th><th>Date</th><th>Edit</th><th>Cancel</th></tr>');


            $.each(results, function (index, t) {
                if (t.completed === true) {
                    $('#manageServicesTable').append('<tr><td>' + t.serviceBookingID + '</td><td>' + t.customerName + '</td><td>' + t.car + '</td><td>' + t.dealershipName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/viewService#type=ServiceID&serviceID=' + t.serviceBookingID + '">View Service</a></td><td></td></tr>');

                } else {
                    $('#manageServicesTable').append('<tr><td>' + t.serviceBookingID + '</td><td>' + t.customerName + '</td><td>' + t.car + '</td><td>' + t.dealershipName + '</td><td>' + t.dateBooked + '</td><td><a href="#!/bookService#type=editExisting&serviceID=' + t.serviceBookingID + '">Edit Service</a></td><td><a id="cancelServiceBooking-' + t.serviceBookingID + '">Cancel Booking</a></td></tr>');

                    $('#cancelServiceBooking-' + t.serviceBookingID).click(function () {
                        var accept = confirm("Do you want to continue?");
                        if (accept) {
                            $.ajax({
                                method: "POST",
                                url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/UpdateServiceBooking',
                                dataType: 'Json',
                                data: '{"ServiceBookingID" : ' + t.serviceBookingID + ',"CustomerID": ' + t.customerID + ',"CarID": ' + t.carID + ',"StaffID": ' + t.staffID + ',"DateBooked": "' + t.dateBooked + '", "DealershipName" : "' + t.dealershipName + '","Active": false}',
                                success: function (results) {
                                    managementManageServices(userData);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

Sales.controller('bookServicesController', function ($scope, $http) {
    var existData = "";
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {

        var date = new Date();
        var days = new Date(2018, (date.getMonth() + 1), 0).getDate();

        console.log(days);

        $('#bookService-Month').empty();
        for (var i = (date.getMonth() + 1); i < 13; i++) {
            if (i.toString().length === 1) {
                $('#bookService-Month').append('<option value="0' + i + '">0' + i + '</option>');
            }
            else {
                $('#bookService-Month').append('<option value="' + i + '">' + i + '</option>');
            }

        }

        $('#bookService-Day').empty();
        for (var i = (date.getDate() + 1); i < days + 1; i++) {
            if (i.toString().length === 1) {
                $('#bookService-Day').append('<option value="0' + i + '">0' + i + '</option>');
            }
            else {
                $('#bookService-Day').append('<option value="' + i + '">' + i + '</option>');
            }

        }

        var location_vars = [];
        var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
        location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
        for (i = 0; i < location_vars_temp.length; i++) {
            location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
            location_vars.push(location_var_key_val[1]);
        }

        $("#bookServiceForm").validate({
            rules: {
                customerSelect: {
                    required: true
                }
            },
            messages: {
                customerSelect: {
                    required: "Please select a customer."
                }
            }
        });

        if (location_vars[0] == undefined) {
            console.log("No Car ID");
        }
        else if (location_vars[0] == "editExisting") {
            console.log("editExisting");
            $("#bookService-firstName").prop('disabled', true);
            $("#bookService-LastName").prop('disabled', true);
            $("#bookService-City").prop('disabled', true);
            $("#bookService-PostCode").prop('disabled', true);
            $("#bookService-FindCustomer").prop('disabled', true);
            $("#bookService-CustomerSelect").prop('disabled', true);
            $.ajax({
                method: "POST",
                url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/ById',
                dataType: 'Json',
                data: '{"ServiceBookingID" : ' + location_vars[1] + '}',
                success: function (results) {
                    existData = results[0];
                    console.log(results[0]);

                    $('#bookService-Dealership').val(results[0].dealershipName);

                    var ajax = $.ajax({
                        method: "POST",
                        url: 'https://teesmo-carcomponent.azurewebsites.net/cc/ByCustomerID',
                        dataType: 'Json',
                        data: '{ "customerID": ' + results[0].customerID + ' }',
                        success: function (results) {
                            console.log(results);
                            $('#bookService-Car').empty();
                            $.each(results, function (index, t) {
                                $('#bookService-Car').append('<option value=' + t.carID + '>' + t.car + '</option>');
                            });
                            $('#bookService-Car').val(results[0].carID);
                        }
                    });

                    var dateBooked = (results[0].dateBooked).split("-");
                    console.log(dateBooked);
                    var day = dateBooked[2].substring(0, 2);
                    var time = dateBooked[2].split("T");
                    var timeCompo = time[1].split(":");
                    var month = dateBooked[1];
                    var hour = timeCompo[0];
                    var minute = timeCompo[1];

                    console.log(day);
                    console.log(month);
                    console.log(hour);
                    console.log(minute);

                    $('#bookService-Day').val(day);
                    $('#bookService-Month').val(month);
                    $('#bookService-Hour').val(hour);
                    $('#bookService-Minute').val(minute);
                }
            });
        }

        $('#bookService-FindCustomer').click(function () {
            var firstName = $('#bookService-firstName').val();
            var lastName = $('#bookService-LastName').val();
            var city = $('#bookService-City').val();
            var postCode = $('#bookService-PostCode').val();
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-profilecomponent.azurewebsites.net/CP/search',
                dataType: 'Json',
                data: '{  "StaffUserName": "' + userData[0].staffUserName + '","StaffPassword": "' + decrypted.toString(CryptoJS.enc.Utf8) +
                '","FirstName": "' + firstName + '","LastName": "' + lastName +
                '","Town": "' + city + '","Postcode": "' + postCode + '"}',
                success: function (results) {
                    console.log(results);
                    $('#bookService-CustomerSelect').empty();

                    $.each(results, function (index, t) {
                        $('#bookService-CustomerSelect').append('<option value="' + t.customerID + '">' + t.customerUserName + '</option>');
                    });

                    $('#bookService-CustomerSelect').change();
                }
            });
        });

        $('#bookService-CustomerSelect')
            .change(function () {
                $("#bookService-CustomerSelect option:selected").each(function () {
                    console.log($(this).val());
                    var iD = $(this).val();

                    var ajax = $.ajax({
                        method: "POST",
                        url: 'https://teesmo-carcomponent.azurewebsites.net/cc/ByCustomerID',
                        dataType: 'Json',
                        data: '{ "customerID": ' + iD + ' }',
                        success: function (results) {
                            console.log(results);
                            $('#bookService-Car').empty();
                            $.each(results, function (index, t) {
                                $('#bookService-Car').append('<option value=' + t.carID + '>' + t.car + '</option>');
                            });
                            $('#bookService-Car').val(results[0].carID);
                        }
                    });
                });
            })
            .change();

        $('#bookService-button').click(function () {
            if ($("#bookServiceForm").valid() === false) {
                alert("Invalid Input");
            }
            else {
                if (location_vars[0] == "editExisting") {
                    console.log("editExisting");
                    var customerID = existData.customerID;
                    var carID = $('#bookService-Car').val();
                    var dateBooked = $('#bookService-Year').val() + "-" + $('#bookService-Month').val() + "-" + $('#bookService-Day').val() + ' ' + $('#bookService-Hour').val() + ":" + $('#bookService-Minute').val();
                    console.log(existData);
                    $.ajax({
                        method: "POST",
                        url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/UpdateServiceBooking',
                        dataType: 'Json',
                        data: '{"ServiceBookingID" : ' + existData.serviceBookingID + ',"CustomerID": ' + parseInt(customerID) + ',"CarID": ' + parseInt(carID) + ',"StaffID": ' + parseInt(existData.staffID) + ',"DateBooked": "' + dateBooked + '", "DealershipName" : "' + $('#bookService-Dealership').val() + '","Active": true}',
                        success: function (results) { window.location.href = '#!/manageServices'; }
                    });
                } else {

                    var customerID = $('#bookService-CustomerSelect').val();
                    var carID = $('#bookService-Car').val();
                    var dateBooked = $('#bookService-Year').val() + "-" + $('#bookService-Month').val() + "-" + $('#bookService-Day').val() + ' ' + $('#bookService-Hour').val() + ":" + $('#bookService-Minute').val();

                    console.log(customerID);
                    console.log(carID);
                    console.log(dateBooked);

                    $.ajax({
                        method: "POST",
                        url: 'https://teesmo-bookingcomponent.azurewebsites.net/SB/InsertNewServiceBooking',
                        dataType: 'Json',
                        data: '{"CustomerID" : ' + parseInt(customerID) + ' , "CarID" : ' + parseInt(carID) + ' , "DateBooked" : "' + dateBooked.toString() + '", "DealershipName" : "' + $('#bookService-Dealership').val() + '"}',
                        success: function (results) { window.location.href = '#!/manageServices'; }
                    });
                }
            }
        });
    }
});

Sales.controller('viewServicesController', function ($scope, $http) {
    var existData = "";
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        if (userData[0].position === "Sales") {
            alert("Not Authorised");
            window.location.href = '#!/manageServices';
        }
        else {
            var location_vars = [];
            var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
            location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
            for (i = 0; i < location_vars_temp.length; i++) {
                location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
                location_vars.push(location_var_key_val[1]);
            }
            var url;
            var data;
            if (location_vars[0] === "ServiceID") {

                url = 'https://teesmo-bookingcomponent.azurewebsites.net/SB/ById';
                data = '{"ServiceBookingID" : ' + location_vars[1] + '}';
            } else if (location_vars[0] === "InvoiceID") {
                url = 'https://teesmo-bookingcomponent.azurewebsites.net/SB/ByInvoice';
                data = '{"InvoiceID" : ' + location_vars[1] + '}';
            }

            $.ajax({
                method: "POST",
                url: url,
                dataType: 'Json',
                data: data,
                success: function (results) {
                    console.log(results[0]);

                    $('#viewServiceContainerDiv').empty();
                    $('#viewServiceContainerDiv').append('<p>Car: <a id="viewCar-' + results[0].carID + '" href="#!/carView#type=viewCar&carID=' + results[0].carID + '">' + results[0].car + '</a></p>\
                        <p>Customer: '+ results[0].customerName + '</p>\
                        <p>Location: '+ results[0].dealershipName + '</p>\
                        <p>Date: '+ (results[0].dateBooked) + '</p><br/><br/>\
                        <table id="viewService-table"></table>');

                    $.ajax({
                        method: "POST",
                        url: 'https://teesmo-servicescomponent.azurewebsites.net/MS/CompletedServiceLogsByServiceBookingID',
                        dataType: 'Json',
                        data: '{  "ServiceBookingID": ' + results[0].serviceBookingID + '}',
                        success: function (results) {
                            console.log(results);
                            $('#viewService-table').empty();
                            $('#viewService-table').append('<tr><th>#</th><th>Staff Member</th><th>Process</th><th>Time (hours)</th><th>Notes</th><th>Amount</th><th>Additional Cost</th></tr>');

                            $.each(results, function (index, t) {
                                $('#viewService-table').append('<tr><td>' + index + '</td><td>' + t.staffName + '</td><td>' + t.serviceProcess + '</td><td>' + t.time + '</td><td>' + t.notes + '</td><td>' + t.amount + '</td><td>' + t.additionalCost + '</td></tr>');
                            });
                        }
                    });
                }
            });
        }
    }
});

Sales.controller('expensesController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        $('#expense-ViewBalance').hide();
        if (userData[0].position === "Admin") {
            $('#expense-ViewBalance').show();
            adminExpenses(userData);
        }
        else {
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-financialcomponent.azurewebsites.net/Expense/ByStaffID',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","StaffID": "' + userData[0].staffID + '"}',
                success: function (results) {
                    console.log(results);
                    $('#expensesTable').empty();
                    $('#expensesTable').append('<tr><th>#</th><th>Expense Type</th><th>Amount</th><th>Date</th><th>Notes</th><th>Status</th></tr>');

                    $.each(results, function (index, t) {
                        $('#expensesTable').append('<tr><td>' + index + '</td><td>' + t.expenseType + '</td><td>' + t.amount + '</td><td>' + (t.date.split('T'))[0] + '</td><td>' + t.notes + '</td><td>' + t.status + '</td></tr>');

                    });
                }
            });
        }
    }
});

function adminExpenses(userData) {
    var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
    $.ajax({
        method: "POST",
        url: 'https://teesmo-financialcomponent.azurewebsites.net/Expense/Expenses',
        dataType: 'Json',
        data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
        success: function (results) {
            console.log(results);
            $('#expensesTable').empty();
            $('#expensesTable').append('<tr><th>#</th><th>Expense Type</th><th>Amount</th><th>Staff Member</th><th>Date</th><th>Notes</th><th>Status</th></tr>');

            $.each(results, function (index, t) {

                if (t.status === "Pending") {
                    $('#expensesTable').append('<tr><td>' + index + '</td><td>' + t.expenseType + '</td><td>' + t.amount + '</td><td>' + t.staffName + '</td><td>' + (t.date.split('T'))[0] + '</td><td>' + t.notes + '</td><td>' + t.status + '</td><td><a id="approveExpense-' + t.expenseID + '">Approve Expense</a></td><td><a id="rejectExpense-' + t.expenseID + '">Reject Expense</a></td></tr>');

                    $('#approveExpense-' + t.expenseID).click(function () {
                        
                        var accept = confirm("Do you want to continue?");
                        if (accept) {
                            var decrypted2 = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                            $.ajax({
                                method: "POST",
                                url: 'https://teesmo-financialcomponent.azurewebsites.net/Expense/ApproveExpense',
                                dataType: 'Json',
                                data: '{"UserName" : "' + userData[0].staffUserName + '","Password": "' + decrypted2.toString(CryptoJS.enc.Utf8) + '","ExpenseID": ' + t.expenseID + '}',
                                success: function (results) { adminExpenses(userData) }
                            });
                        }
                    });

                    $('#rejectExpense-' + t.expenseID).click(function () {
                        var accept = confirm("Do you want to continue?");
                        if (accept) {
                            var decrypted2 = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                            $.ajax({
                                method: "POST",
                                url: 'https://teesmo-financialcomponent.azurewebsites.net/Expense/RejectExpense',
                                dataType: 'Json',
                                data: '{"UserName" : "' + userData[0].staffUserName + '","Password": "' + decrypted2.toString(CryptoJS.enc.Utf8) + '","ExpenseID": ' + t.expenseID + '}',
                                success: function (results) { adminExpenses(userData) }
                            });
                        }
                    });
                }
                else {
                    $('#expensesTable').append('<tr><td>' + index + '</td><td>' + t.expenseType + '</td><td>' + t.amount + '</td><td>' + t.staffName + '</td><td>' + (t.date.split('T'))[0] + '</td><td>' + t.notes + '</td><td>' + t.status + '</td><td></td><td></td></tr>');
                }
            });
        }
    });
};


Sales.controller('expensesFileController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        $("#fileExpenseContainerDiv").validate({
            rules: {
                amount: {
                    number: true,
                    maxlength: 7,
                    required: true
                },
                notes: {
                    maxlength: 200,
                    required: true
                }
            },
            messages: {
                amount: {
                    required: "Please specify an amount.",
                    number: "Must be a number."
                },
                notes: {
                    required: "Please specify the nature of the expense."
                }
            }
        });

        $('#fileExpense-Button').click(function () {
            if ($("#fileExpenseContainerDiv").valid() === false) {
                alert("Invalid Input");
            }
            else {
                var type = $('#fileExpense-expenseType').find(":selected").val();
                var amount = $('#fileExpense-Amount').val();
                var notes = $('#fileExpense-Notes').val();
                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-financialcomponent.azurewebsites.net/Expense/InsertExpense',
                    dataType: 'Json',
                    data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) +
                    '","ExpenseType": "' + type + '","Amount": ' + parseFloat(amount) + ',"StaffID": ' + userData[0].staffID + ',"Notes": "' + notes + '"}',

                    success: function (results) {
                        window.location.href = '#!/expenses';
                    }
                });
            }
        });
    }
});

Sales.controller('invoicesController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        $('#invoice-ViewBalance').hide();
        if (userData[0].position === "Sales") {
            alert("Unauthorised");
            window.location.href = '#!/expenses';
        }
        else if (userData[0].position === "Admin") {
            $('#invoice-ViewBalance').show();
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-financialcomponent.azurewebsites.net/Invoice/Invoices',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
                success: function (results) {
                    console.log(results);
                    $('#invoicesTable').empty();
                    $('#invoicesTable').append('<tr><th>#</th><th>Customer</th><th>Amount</th><th>Invoice Type</th><th>Date</th></tr>');


                    $.each(results, function (index, t) {
                        var type;
                        if (t.salesID != null) {
                            type = "Sale";
                            $('#invoicesTable').append('<tr><td>' + index + '</td><td>' + t.customerName + '</td><td>' + t.amount + '</td><td>' + type + '</td><td>' + (t.date.split('T'))[0] + '</td><td><a href="#!/salesView#type=InvoiceID&salesID=' + t.invoiceID + '">View Sale</a></td></tr>');
                        }
                        if (t.serviceID != null) {
                            type = "Service";
                            $('#invoicesTable').append('<tr><td>' + index + '</td><td>' + t.customerName + '</td><td>' + t.amount + '</td><td>' + type + '</td><td>' + (t.date.split('T'))[0] + '</td><td><a href="#!/viewService#type=InvoiceID&serviceID=' + t.invoiceID + '">View Service</a></td></tr>');
                        }
                        if (t.tradeID != null) {
                            type = "Trade";
                            $('#invoicesTable').append('<tr><td>' + index + '</td><td>' + t.customerName + '</td><td>' + t.amount + '</td><td>' + type + '</td><td>' + (t.date.split('T'))[0] + '</td><td><a href="#!/tradesView#type=InvoiceID&tradesID=' + t.invoiceID + '">View Trade</a></td></tr>');
                        }
                    });
                }
            });
        }
        else {
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-financialcomponent.azurewebsites.net/Invoice/ByDealership',
                dataType: 'Json',
                data: '{  "UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","DealershipName": "' + userData[0].dealershipName + '"}',
                success: function (results) {
                    console.log(results);
                    $('#invoicesTable').empty();
                    $('#invoicesTable').append('<tr><th>#</th><th>Customer</th><th>Amount</th><th>Invoice Type</th><th>Date</th></tr>');

                    $.each(results, function (index, t) {
                        var type;
                        if (t.salesID != null) {
                            type = "Sale";
                            $('#invoicesTable').append('<tr><td>' + index + '</td><td>' + t.customerName + '</td><td>' + t.amount + '</td><td>' + type + '</td><td>' + (t.date.split('T'))[0] + '</td><td><a href="#!/salesView#type=InvoiceID&salesID=' + t.invoiceID + '">View Sale</a></td></tr>');
                        }
                        if (t.serviceID != null) {
                            type = "Service";
                            $('#invoicesTable').append('<tr><td>' + index + '</td><td>' + t.customerName + '</td><td>' + t.amount + '</td><td>' + type + '</td><td>' + (t.date.split('T'))[0] + '</td><td><a href="#!/viewService#type=InvoiceID&serviceID=' + t.invoiceID + '">View Service</a></td></tr>');
                        }
                        if (t.tradeID != null) {
                            type = "Trade";
                            $('#invoicesTable').append('<tr><td>' + index + '</td><td>' + t.customerName + '</td><td>' + t.amount + '</td><td>' + type + '</td><td>' + (t.date.split('T'))[0] + '</td><td><a href="#!/tradesView#type=InvoiceID&tradesID=' + t.invoiceID + '">View Trade</a></td></tr>');
                        }
                    });
                }
            });
        }
    }
});

Sales.controller('balanceController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        if (userData[0].position === "Admin") {
            var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
            $.ajax({
                method: "POST",
                url: 'https://teesmo-financialcomponent.azurewebsites.net/Balance/Balance',
                dataType: 'Json',
                data: '{"UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
                success: function (results) {
                    $('#balanceDiv').empty();
                    $('#balanceDiv').append('<p>Current Account Balance: ' + results[0] + '</p>');
                }
            });
        }
        else {
            alert("Unauthorised");
            window.location.href = '#!/expenses';
        }
    }
});

Sales.controller('staffMembersController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        if (userData[0].position === "Admin") {
            AdminStaff(userData);
        }
        else if (userData[0].position === "Management") {
            managementStaff(userData);
        }
        else {
            alert("Not Authorised");
            window.location.href = '#!/cars';
        }
    }
});

function AdminStaff(userData) {
    var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
    $.ajax({
        method: "POST",
        url: 'https://teesmo-profilecomponent.azurewebsites.net/SP/All',
        dataType: 'Json',
        data: '{"UserName": "' + userData[0].staffUserName + '","Password": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
        success: function (results) {
            console.log(results);
            $('#staffMembersTable').empty();
            $('#staffMembersTable').append('<tr><th>Name</th><th>Date of Birth</th><th>Email</th><th>Address</th><th>Postcode</th><th>Position</th><th>Dealership</th><th>Salary</th></tr>');
            $.each(results, function (index, t) {
                $('#staffMembersTable').append('<tr><td>' + t.staffName + '</td><td>' + t.doB + '</td><td>' + t.email + '</td><td>' + t.address + '</td>\
                                                        <td>' + t.postcode + '</td><td>' + t.position + '</td><td>' + t.dealershipName + '</td><td>' + t.salary + '</td><td><a id="deleteEmployee-' + t.staffID + '">Delete Employee</a></td></tr>');
                $('#deleteEmployee-' + t.staffID).click(function () {
                    if (userData[0].staffID === t.staffID) {
                        alert("Can't Delete Yourself");
                    } else {
                        var accept = confirm("Do you want to continue?");
                        if (accept) {
                            var decrypted2 = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                            $.ajax({
                                method: "POST",
                                url: 'https://teesmo-profilecomponent.azurewebsites.net/SP/RemoveStaff',
                                dataType: 'Json',
                                data: '{"StaffUserName" : "' + userData[0].staffUserName + '","StaffPassword": "' + decrypted2.toString(CryptoJS.enc.Utf8) + '","StaffID": ' + t.staffID + '}',
                                success: function (results) {
                                    AdminStaff(userData);
                                }
                            });
                        }
                    }
                });
            });
        }
    });
}

function managementStaff(userData) {
    var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
    $.ajax({
        method: "POST",
        url: 'https://teesmo-profilecomponent.azurewebsites.net/SP/StaffByDealership',
        dataType: 'Json',
        data: '{"StaffUserName": "' + userData[0].staffUserName + '","StaffPassword": "' + decrypted.toString(CryptoJS.enc.Utf8) + '","Dealership": "' + userData[0].dealershipName + '"}',
        success: function (results) {
            console.log(results);
            $('#staffMembersTable').empty();
            $('#staffMembersTable').append('<tr><th>Name</th><th>Date of Birth</th><th>Email</th><th>Address</th><th>Postcode</th><th>Position</th><th>Dealership</th><th>Salary</th></tr>');
            $.each(results, function (index, t) {
                $('#staffMembersTable').append('<tr><td>' + t.staffName + '</td><td>' + t.doB + '</td><td>' + t.email + '</td><td>' + t.address + '</td>\
                                                        <td>' + t.postcode + '</td><td>' + t.position + '</td><td>' + t.dealershipName + '</td><td>' + t.salary + '</td><td><a id="deleteEmployee-' + t.staffID + '">Delete Employee</a></td></tr>');
                $('#deleteEmployee-' + t.staffID).click(function () {
                    if (userData[0].staffID === t.staffID) {
                        alert("Can't Delete Yourself");
                    } else {
                        var accept = confirm("Do you want to continue?");
                        if (accept) {
                            var decrypted2 = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                            $.ajax({
                                method: "POST",
                                url: 'https://teesmo-profilecomponent.azurewebsites.net/SP/RemoveStaff',
                                dataType: 'Json',
                                data: '{"StaffUserName" : "' + userData[0].staffUserName + '","StaffPassword": "' + decrypted2.toString(CryptoJS.enc.Utf8) + '","StaffID": ' + t.staffID + '}',
                                success: function (results) {
                                    managementStaff(userData)

                                }
                            });
                        }
                    }
                });
            });
        }
    });
}

Sales.controller('staffMemberAddController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {

        $("#addStaffContainerDiv").validate({
            rules: {
                userName: {
                    required: true,
                    minlength: 8
                },
                password: {
                    minlength: 8,
                    required: true
                },
                firstName: {
                    required: true
                },
                LastName: {
                    required: true
                },
                dateofBirth: {
                    required: true,
                    date: true
                },
                email: {
                    required: true,
                    email: true
                },
                houseNumber: {
                    number: true,
                    minlength: 1,
                    required: true
                },
                address1: {
                    required: true
                },
                address2: {
                    required: false
                },
                city: {
                    required: true
                },
                postCode: {
                    required: true
                },
                salary: {
                    number: true,
                    minlength: 5,
                    required: true
                }
            },
            messages: {
                userName: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 8 characters"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 8 characters long"
                },
                firstName: "Please enter your firstname",
                lastName: "Please enter your lastname",
                dateofBirth: {
                    required: "Please enter your date of birth (DD-MM-YYYY)",
                    date: "Please enter your date of birth (DD-MM-YYYY)"
                },
                email: "Please enter a valid email address",
                houseNumber: {
                    required: "Please enter your door number",
                    number: "Please enter your door number",
                    minlength: "Please enter your door number",
                },
                address1: "Please enter the first line of your address, house number included.",
                address2: "Please enter the second line of your address, if you have one.",
                city: "Please enter the name of your town/city",
                postCode: "Please enter your postcode",
                salary: {
                    required: "Please enter a salary",
                    number: "Must be a number",
                    minlength: "Please enter a salary larger than 5 figures.",
                }
            }
        });

        $('#addStaff-position').empty();

        if (userData[0].position === "Admin") {
            $('#addStaff-position').append($("<option></option>")
                .val("Sales")
                .text("Sales"));
            $('#addStaff-position').append($("<option></option>")
                .val("Mechanic")
                .text("Mechanic"));
            $('#addStaff-position').append($("<option></option>")
                .val("Management")
                .text("Management"));
        }
        else if (userData[0].position === "Management") {
            $('#addStaff-position').append($("<option></option>")
                .val("Sales")
                .text("Sales"));
            $('#addStaff-position').append($("<option></option>")
                .val("Mechanic")
                .text("Mechanic"));
        }
        
        $('#addStaff-Button').click(function () {
            if ($("#addStaffContainerDiv").valid() === false) {
                alert("Invalid Input");
            } else {
                var userName = $('#addStaff-userName').val();
                var password = $('#addStaff-password').val();
                var firstName = $('#addStaff-firstName').val();
                var LastName = $('#addStaff-LastName').val();
                var dateofBirth = $('#addStaff-dateofBirth').val();
                var email = $('#addStaff-email').val();
                var houseNumber = $('#addStaff-houseNumber').val();
                var address1 = $('#addStaff-address1').val();
                var address2 = $('#addStaff-address2').val();
                var city = $('#addStaff-city').val();
                var postCode = $('#addStaff-postCode').val().toUpperCase();
                var position = $('#addStaff-position').val();
                var salary = $('#addStaff-salary').val();
                var Dealership = $('#addStaff-Dealership').val();

                var date = new Date(dateofBirth),
                    month = '' + (date.getMonth() + 1),
                    day = '' + date.getDate(),
                    year = date.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                var dateString = [year, month, day].join('-');

                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-profilecomponent.azurewebsites.net/SP/InsertNewStaff',
                    dataType: 'Json',
                    data: '{"StaffUserName" : "' + userData[0].staffUserName + '","StaffPassword" : "' + decrypted.toString(CryptoJS.enc.Utf8) + '","NewStaffUserName" : "' + userName + '","NewStaffPassword" : "' + password +
                    '","FirstName" : "' + firstName + '","LastName" : "' + LastName + '","DoB" : "' + dateString + '","Email" : "' + email + '","HouseNumber" : ' + parseInt(houseNumber) + ',"Address1" : "' + address1 +
                    '","Address2" : "' + address2 + '","Town" : "' + city + '","Postcode" : "' + postCode + '","Position" : "' + position + '","Salary" : ' + parseFloat(salary) + ',"DealershipName" : "' + Dealership + '"}',
                    success: function (results) {
                        window.location.href = '#!/staffMembers';
                    }
                });
            }
        });
    }
});

Sales.controller('myAccountController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {
        console.log(userData[0]);
        $('#staffAccount-userName').val(userData[0].staffUserName);
        $('#staffAccount-firstName').val(userData[0].firstName);
        $('#staffAccount-lastName').val(userData[0].lastName);
        $('#staffAccount-dateofBirth').val(userData[0].doB)
        $('#staffAccount-email').val(userData[0].email);
        $('#staffAccount-number').val(userData[0].houseNumber);
        $('#staffAccount-address1').val(userData[0].address1);
        $('#staffAccount-address2').val(userData[0].address2);
        $('#staffAccount-city').val(userData[0].town);
        $('#staffAccount-postcode').val(userData[0].postcode);
        $('#staffAccount-position').val(userData[0].position);
        $('#staffAccount-salary').val(userData[0].salary);
        $('#staffAccount-dealership').val(userData[0].dealershipName);

    }
});

Sales.controller('editAccountController', function ($scope, $http) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    if (userData === null) {
        window.location.href = '#!/login';
    }
    else {

        $("#editAccountDiv").validate({
            rules: {
                firstName: {
                    required: true
                },
                lastName: {
                    required: true
                },
                dateofBirth: {
                    required: true,
                    date: true
                },
                email: {
                    required: true,
                    email: true
                },
                houseNumber: {
                    number: true,
                    minlength: 1,
                    required: true
                },
                address1: {
                    required: true
                },
                address2: {
                    required: false
                },
                city: {
                    required: true
                },
                postCode: {
                    required: true
                }
            },
            messages: {
                firstName: "Please enter your firstname",
                lastName: "Please enter your lastname",
                dateofBirth: {
                    required: "Please enter your date of birth (DD-MM-YYYY)",
                    date: "Please enter your date of birth (DD-MM-YYYY)"
                },
                email: "Please enter a valid email address",
                houseNumber: {
                    required: "Please enter your door number",
                    number: "Please enter your door number",
                    minlength: "Please enter your door number",
                },
                address1: "Please enter the first line of your address, house number included.",
                address2: "Please enter the second line of your address, if you have one.",
                city: "Please enter the name of your town/city",
                postCode: "Please enter your postcode"
            }
        });

        console.log(userData[0]);
        $('#editAccount-firstName').val(userData[0].firstName);
        $('#editAccount-lastName').val(userData[0].lastName);
        $('#editAccount-dateofBirth').val(userData[0].doB)
        $('#editAccount-email').val(userData[0].email);
        $('#editAccount-houseNumber').val(userData[0].houseNumber);
        $('#editAccount-address1').val(userData[0].address1);
        $('#editAccount-address2').val(userData[0].address2);
        $('#editAccount-city').val(userData[0].town);
        $('#editAccount-postCode').val(userData[0].postcode);
        
        $('#editAccount-Button').click(function () {
            if ($("#editAccountDiv").valid() === false) {
                alert("Invalid Input");
            }
            else {
                var firstName = $('#editAccount-firstName').val();
                var LastName = $('#editAccount-lastName').val();
                var dateofBirth = $('#editAccount-dateofBirth').val();
                var email = $('#editAccount-email').val();
                var houseNumber = $('#editAccount-houseNumber').val();
                var address1 = $('#editAccount-address1').val();
                var address2 = $('#editAccount-address2').val();
                var city = $('#editAccount-city').val();
                var postCode = $('#editAccount-postCode').val().toUpperCase();

                var date = new Date(dateofBirth),
                    month = '' + (date.getMonth() + 1),
                    day = '' + date.getDate(),
                    year = date.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                var dateString = [year, month, day].join('-');
                var decrypted = CryptoJS.AES.decrypt(sessionStorage.getItem("password"), sessionStorage.getItem("secretKey"));
                $.ajax({
                    method: "POST",
                    url: 'https://teesmo-profilecomponent.azurewebsites.net/SP/UpdateStaff',
                    dataType: 'Json',
                    data: '{"StaffUserName" : "' + userData[0].staffUserName + '","StaffPassword" : "' + decrypted.toString(CryptoJS.enc.Utf8) + '","StaffID" : "' + userData[0].staffID +
                    '","FirstName" : "' + firstName + '","LastName" : "' + LastName + '","DoB" : "' + dateString + '","Email" : "' + email + '","HouseNumber" : ' + parseInt(houseNumber) + ',"Address1" : "' + address1 +
                    '","Address2" : "' + address2 + '","Town" : "' + city + '","Postcode" : "' + postCode + '","Position" : "' + userData[0].position + '","Salary" : ' + userData[0].salary + ',"DealershipName" : "' + userData[0].dealershipName + '"}',
                    success: function (results) {
                        $.ajax({
                            method: "POST",
                            url: 'https://teesmo-profilecomponent.azurewebsites.net/SP/login',
                            dataType: 'Json',
                            data: '{"StaffUserName": "' + userData[0].staffUserName + '","StaffPassword": "' + decrypted.toString(CryptoJS.enc.Utf8) + '"}',
                            success: function (userData) {
                                sessionStorage.setItem("userData", JSON.stringify(userData));
                                window.location.href = '#!/myAccount';
                            }
                        });
                    }
                });
            }
        });
    }
});

Sales.controller('createCustomerAccountController', function ($scope, $http) {
    var location_vars = [];
    var location_vars_temp = location.hash.replace('#', ''); // Remove the hash sign
    location_vars_temp = location_vars_temp.split('&'); // Break down to key-value pairs
    for (i = 0; i < location_vars_temp.length; i++) {
        location_var_key_val = location_vars_temp[i].split('='); // Break down each pair
        location_vars.push(location_var_key_val[1]);
    }

    $("#createCustomerAccountDiv").validate({
        rules: {
            userName: {
                required: true,
                minlength: 8
            },
            password: {
                minlength: 8,
                required: true
            },
            firstName: {
                required: true
            },
            lastName: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            number: {
                number: true,
                minlength: 8,
                required: true
            },
            address1: {
                required: true
            },
            address2: {
                required: false
            },
            city: {
                required: true
            },
            postcode: {
                required: true
            }
        },
        messages: {
            userName: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 8 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 8 characters long"
            },
            firstName: "Please enter your firstname",
            lastName: "Please enter your lastname",
            email: "Please enter a valid email address",
            number: {
                required: "Please enter your phone number",
                number: "Please enter your phone number",
                minlength: "Please enter your phone number",
            },
            address1: "Please enter the first line of your address, house number included.",
            address2: "Please enter the second line of your address, if you have one.",
            city: "Please enter the name of your town/city",
            postcode: "Please enter your postcode"
        }
    });

    $('#newCustomerCreateButton').click(function () {
        if ($("#createCustomerAccountDiv").valid() === false) {
            alert("Invalid Input");
        }
        else {

            $.ajax({
                method: "POST",
                url: 'https://teesmo-profilecomponent.azurewebsites.net/CP/InsertNewCustomer',
                dataType: 'Json',
                data: '{"CustomerUserName": "' + $('#newCustomer-userName').val() + '","CustomerPassword": "' + $('#newCustomer-password').val() + '","FirstName": "' + $('#newCustomer-firstName').val() +
                '","LastName": "' + $('#newCustomer-lastName').val() + '","Email": "' + $('#newCustomer-email').val() + '","HouseNumber": "' + $('#newCustomer-number').val() +
                '","Address1": "' + userData[0].address1 + '","Address2": "' + userData[0].address2 + '","Town": "' + $('#newCustomer-city').val() + '","Postcode": "' + $('#newCustomer-postcode').val() + '"}',
                success: function () {

                    if (location_vars[0] === "sell") {
                        window.location.href = '#!/sell';
                    } else if (location_vars[0] === "trade") {
                        window.location.href = '#!/trade';
                    } else if (location_vars[0] === "testDrive") {
                        window.location.href = '#!/bookTestDrive';
                    } else if (location_vars[0] === "service") {
                        window.location.href = '#!/bookService';
                    }
                }
            });
        }
    });
});

function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789[]{};:'@#~,<.>/?!�$%^&*()_-=+";

    for (var i = 0; i < 100; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}