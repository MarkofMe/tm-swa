namespace CreditCardMock

open System
open FSharp.Core
open System.Net

module CreditCardMock =

    type mockCreditCardData = {
                        mutable CardNumber : string
                        mutable AccountNumber : string
                        mutable SortCode : string
                        mutable ExpiryDate : string
                        mutable NameOnCard : string
                        mutable CV2 : string
                        mutable Active : bool
                    }
    
    let private emptyMockCreditCardData = {
                                            CardNumber = ""
                                            AccountNumber = ""
                                            SortCode = ""
                                            ExpiryDate = ""
                                            NameOnCard = ""
                                            CV2 = ""
                                            Active = false
                                          }

    type displayMockCreditCardData = {
                        CardNumber : string
                        HashCard : string
                        RawCardNumber : string
                        ExpiryDate : string
                        NameOnCard : string
                        Active : bool
                    }
    
    let private emptyDisplayMockCreditCardData = {
                                                    CardNumber = ""
                                                    HashCard = ""
                                                    RawCardNumber = ""
                                                    ExpiryDate = ""
                                                    NameOnCard = ""
                                                    Active = false
                                                  }

    let mutable mockCreditCardData = 
        [|
            {CardNumber = "0000000000000000"
             AccountNumber = "00000000"
             SortCode = "000000"
             ExpiryDate = "04/19"
             NameOnCard = "testy testerson               "
             CV2 = "000"
             Active = true};
            {CardNumber = "1234567891234567"
             AccountNumber = "12345678"
             SortCode = "654321"
             ExpiryDate = "12/19"
             NameOnCard = "Mr T hAmCo                    "
             CV2 = "321"
             Active = true};
            {CardNumber = "9876543210987654"
             AccountNumber = "98765432"
             SortCode = "987654"
             ExpiryDate = "01/18"
             NameOnCard = "M J Leon                      "
             CV2 = "987"
             Active = true}
        |]
    
    let getAllMock() =
        try
            mockCreditCardData |> Array.map (fun row -> {
                                                        CardNumber = row.CardNumber
                                                        AccountNumber = row.AccountNumber
                                                        SortCode = row.SortCode
                                                        ExpiryDate = row.ExpiryDate
                                                        NameOnCard = row.NameOnCard
                                                        CV2 = row.CV2
                                                        Active = row.Active
                                                        })
        with ex ->
            [|emptyMockCreditCardData|]

    let hideMockCardNumber( cardNumber : string) =
        let cutDown = cardNumber.Substring(cardNumber.Length - 4)
        let stars = "************"
        stars + cutDown
    
    let getStaffDisplayMockCards() =
        try
            mockCreditCardData |> Array.map (fun row -> 
                                          match row.Active with
                                          | true -> 
                                                   {
                                                    CardNumber = hideMockCardNumber(row.CardNumber)
                                                    HashCard = (hash row.CardNumber).ToString()
                                                    RawCardNumber = row.CardNumber
                                                    ExpiryDate = row.ExpiryDate
                                                    NameOnCard = row.NameOnCard
                                                    Active = row.Active
                                                    } |> Some
                                          |_ -> None
                                            ) |> Array.choose id
        with ex ->
            [|emptyDisplayMockCreditCardData|]
    
    let filterHashedMockCard (hashedCard : string) =
        try
            let data =
                mockCreditCardData |> Seq.map (fun row -> match ((hash row.CardNumber).ToString()).Equals(hashedCard) with
                                                          | true -> {
                                                                      CardNumber = row.CardNumber
                                                                      AccountNumber = row.AccountNumber
                                                                      SortCode = row.SortCode
                                                                      ExpiryDate = row.ExpiryDate
                                                                      NameOnCard = row.NameOnCard
                                                                      CV2 = row.CV2
                                                                      Active = row.Active
                                                                     } |> Some
                                                          | _-> None
                              )
            (data |> Seq.toArray |> Array.choose id)
        with ex ->
            [|emptyMockCreditCardData|]

    let getMockCardDetails (hashedCardNumber : string) =
        try
            filterHashedMockCard hashedCardNumber
        with ex ->
            [|emptyMockCreditCardData|]

    let insertNewMockCreditCard (data : string[]) = 
        try
            let creditCardValues = [|{
                                        CardNumber = data.GetValue(0).ToString()
                                        AccountNumber = data.GetValue(1).ToString()
                                        SortCode = data.GetValue(2).ToString()
                                        ExpiryDate = data.GetValue(3).ToString()
                                        NameOnCard = data.GetValue(4).ToString()
                                        CV2 = data.GetValue(5).ToString()
                                        Active = true
                                        }|]
            
            mockCreditCardData <- mockCreditCardData |> Array.append creditCardValues
            HttpStatusCode.OK
        with ex ->
            HttpStatusCode.InternalServerError
    
    let filterMockCard (card : string) =
        try 
            let data =
                mockCreditCardData |> Seq.map (fun row -> match ((row.CardNumber).ToString()).Equals(card) with
                                                          | true -> {
                                                                          CardNumber = row.CardNumber
                                                                          AccountNumber = row.AccountNumber
                                                                          SortCode = row.SortCode
                                                                          ExpiryDate = row.ExpiryDate
                                                                          NameOnCard = row.NameOnCard
                                                                          CV2 = row.CV2
                                                                          Active = row.Active
                                                                          } |> Some
                                                          | _-> None
                              )
            (data |> Seq.toArray |> Array.choose id)
        with ex -> 
            [|emptyMockCreditCardData|]

    let updateMockCreditCard (data : string[]) = 
        try
            let mutable card = (filterMockCard data.[0]).[0]
        
            card.AccountNumber <- data.[1]
            card.SortCode <- data.[2]
            card.ExpiryDate <- data.[3]
            card.NameOnCard <- data.[4]
            card.CV2 <- data.[5]
            card.Active <- match data.[6] with
                           | "true" -> true
                           |_ -> false
        
            let i = mockCreditCardData |> Array.findIndex (fun i -> i.AccountNumber.Equals(card.AccountNumber))

            Array.set mockCreditCardData i card
            HttpStatusCode.OK
        with ex ->
            HttpStatusCode.InternalServerError