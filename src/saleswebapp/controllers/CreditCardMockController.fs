﻿namespace CreditCardMock.Controllers

open System
open System.Net.Http
open FSharp.Core
open System.Web.Http
open CreditCardMock
open System.Net


[<RoutePrefixAttribute("CreditCardMock")>]
type CreditCardController() =
    inherit ApiController()

    [<Route("GetAllMock"); HttpGet>]
    member this.GetAll () =
        System.Diagnostics.Debug.WriteLine("Called data")
        CreditCardMock.getAllMock()
        // /CC/GetAll
    
    [<Route("GetStaffMockCards"); HttpGet>]
    member this.GetStaffCards () =
        System.Diagnostics.Debug.WriteLine("Called data")
        CreditCardMock.getStaffDisplayMockCards()
        // /CC/GetStaffCards

    [<Route("InsertNewMockCard")>]
    member this.PostInsert (insert : HttpRequestMessage) : HttpStatusCode =
        try
            let content = insert.Content
            let jsonContent = content.ReadAsFormDataAsync().Result
            let mutable data = jsonContent.GetValues(0)
            System.Diagnostics.Debug.WriteLine("Called method insert production data into development")
            CreditCardMock.insertNewMockCreditCard data
            // /CC/InsertNewCard
        with ex ->
            HttpStatusCode.InternalServerError
            

    [<Route("UpdateMockCard")>]
    member this.UpdateCard (insert : HttpRequestMessage) : HttpStatusCode =
        try
            let content = insert.Content
            let jsonContent = content.ReadAsFormDataAsync().Result
            let mutable data = jsonContent.GetValues(0)
            System.Diagnostics.Debug.WriteLine("Called method insert production data into development")
            CreditCardMock.updateMockCreditCard(data)
        with ex ->
            HttpStatusCode.InternalServerError
        
    
    [<Route("GetMockCardDetails")>]
    member this.GetCardDetails (cardDetails : string) =
        //let content = ac.GetQueryNameValuePairs() |> Seq.map(fun a -> a.Value) |> Seq.toList
        CreditCardMock.getMockCardDetails cardDetails // content.[0]
